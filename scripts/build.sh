#!/bin/bash

# #!/usr/bin/env bash
echo "Executing command to remove dangling image: images with <none>:<none>"
docker rmi $(docker images --filter="dangling=true")