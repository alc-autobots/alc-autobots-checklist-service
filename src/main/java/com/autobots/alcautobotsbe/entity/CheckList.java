package com.autobots.alcautobotsbe.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CheckList {
    @Id
    /* deep dive meeting */
    private String Id;
    private boolean sendQuestionnaire;
    private boolean createHubPage;
    private boolean scheduleDeepDiveMeeting;
    private boolean determineCustomer;
    private boolean disscussAddons;
    private boolean proxySetting;
    private boolean trendExport;
    private boolean explainCloudMigrationTool;
    private boolean networking;
    private boolean changeCloud;
    private boolean actionUi;
    private boolean patching;
    private boolean siteBuilder;
    private boolean workArounds;
    private boolean sourceTrees;
    private boolean gap;
    private boolean coming;
    private boolean passwordNotify;
    private boolean methodToMinimize;
    private boolean determineNetworking;
    private boolean changeRoute;
    private boolean testingValidation;
    private boolean targetDates;
    private boolean addMember;

    /* upgrade system */
    private boolean webCtrlAccess;
    private boolean webCtrlKey;
    private boolean bacLicenses;
    private boolean upgradeSystem;
    private boolean cleanOutWebRoot;
    private boolean notUsingDerby;
    private boolean scheduleMigration;

    /* update drivers */
    private boolean updateAllDevice;

    /* tosibox setup */
    private boolean orderTosibox;
    private boolean digitalNetTicket;
    private boolean staticIpAddress;
    private boolean switchPort;
    private boolean firewallRules;
    private boolean configureTosibox;
    private boolean shipTosibox;
    private boolean veryfyTosiboxbyOwner;

    /* cloud system setup */
    private boolean implementAwsNetwork;
    private boolean createEmptyAwsSystem;
    private boolean giveToolAndPassword;
    private boolean giveOwnerWebCtrlUrl;

    /* owner pre-migration */
    private boolean updateBbmd;
    private boolean changeDefaultRoute;
    private boolean makeBbmdDevice;

    /* process Improvement */
    @Column(name="testPotential")
    private boolean automatedTestPotential;
    @Column(name="dbTrend")
    private boolean dbTrendSetting;
    private boolean rngTrendNo;
    private boolean programToDownload;
    private boolean createAlarm;
    private boolean jordanToSoap;
    @Column(name="autoTest")
    private boolean askSteveForAutoTest;

    /* migrate to aws */
    @Column(name="webCtrlOA")
    private boolean webCtrlOwnerAction;
    private boolean disableLdap;
    private boolean stopControllerConnection;
    private boolean systemShutDown;
    private boolean runCloudTool;
    private boolean danDocSize;
    private boolean copyAndRunTool;
    private boolean copyArtifact;
    private boolean fileToEfs;
    private boolean navigateToEsfFile;
    private boolean runAwsS3;

    private boolean importAndConfigData;
    private boolean editContainerProps;
    private boolean stopWebCtrlTask;

    private boolean testSystemWithStopConn;
    private boolean verifyAddons;
    private boolean changeProxySet;
    private boolean checkTrendExport;
    private boolean walkThroughUi;
    private boolean clickStatisticBtn;
    private boolean checkOperator;
    private boolean viewAlarms;
    private boolean runSecurity;
    private boolean viewOneTrend;
    private boolean confimServiceStatus;
    private boolean exportTrend;
    private boolean danPingBbmd;

    @Column(name="ipSetting")
    private boolean changeIpSetting;
    private boolean keepOnlyPort;
    private boolean forceRegistration;
    private boolean enableDevice;
    private boolean dontUseBackUpBbmd;
    private boolean tuning;
    private boolean timeOut6s;
    @Column(name="attempts10s")
    private boolean attempts10;
    private boolean sync72m;
    private boolean registerFd;

    private boolean startConn;

    private boolean confirmConnectivity;
    private boolean seeColors;
    private boolean runModstat;
    private boolean seeDataOnEquipment;
    @Column(name="trendCount")
    private boolean captureTrendCount;
    private boolean showDbValues;
    private boolean pointLockAlarm;
    private boolean configureTrend1m;
    private boolean enableTrendHistorian;
    private boolean forceAlarm;
    private boolean verifyDeviceBind;

    private boolean postLaunchWebCtrl;
    private boolean changeServerEmail;
    @Column(name="mailSetup")
    private boolean changeMailSetup;
    private boolean changePort;
    private boolean secureTsl;
    @Column(name="mailUser")
    private boolean addMailUser;
    @Column(name="changeEmail")
    private boolean changeEmailAddress;

    private boolean testSystemWithOwner;
    private boolean emailNotification;
}
