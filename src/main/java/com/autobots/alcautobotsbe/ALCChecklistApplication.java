package com.autobots.alcautobotsbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableEurekaClient do not remove
public class ALCChecklistApplication {
    public static void main(String[] args) {
        SpringApplication.run(ALCChecklistApplication.class, args);
    }
}
