profile "local" is using H2 Database and profile "dev" is using AWS RDS MySQL

local profile run on port : http://localhost:7070

dev profile run on port : http://localhost:5000

Two ways to SWITCH environtment: 
1. by using mvn command:

       mvn spring-boot:run -Dspring-boot.run.profiles={profile.name}

2. specified on application.properties file:

      spring.profiles.active={profile.name}

